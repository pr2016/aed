% Cuarto Set
% Carlos Pita
% 29 de Abril de 2016

# Ejercicio 7

## Punto a

Los coeficientes resultantes de la regresión multilineal de balance contra los
regresores pedidos se listan en la siguiente tabla:


  Variable            Coeficiente
  -----------      --------------
  Término fijo          -516.7183 
  Income                  -7.9446
  Limit                    0.1216
  Rating                   2.1904
  Student                422.6684

## Punto b

Agregando dos variables binarias para codificar la etnia, los coeficientes pasan
a ser:

  Variable       Coeficiente
  -----------  -------------
  Término fijo     -527.8887
  Income             -7.9433
  Limit               0.1203
  Rating              2.2118
  Student           421.9849
  Asian              18.7897
  Caucasian          10.1769

## Punto c

Finalmente, se presentan los coeficientes para cada subconjunto de regresores
del conjunto $\{Income, Rating, Student\}$ (incluyendo en cada caso un término
fijo) y el correspondiente error cuadrático medio estimado sobre $1/3$ de los
datos:

  Modelo     Término fijo       Income       Rating      Student        Error
----------  -------------    ---------    ---------   ----------    ---------
   c.1          -581.0026      -8.0125       3.9931      430.417      10945.2
   c.2           224.9985       5.4045          ---      409.653     145577.6
   c.3          -446.6246         ---        2.5825      396.584      35543.2
   c.4          -544.2224      -7.6591       3.9703        ---        23564.3
   c.5           255.6333       5.6680          ---        ---       152763.7
   c.6          -418.0969         ---        2.6190        ---        49166.3
   c.7           473.2343         ---           ---      453.408     209225.4
   c.8           520.7828         ---           ---        ---       212146.0

El modelo con mejor desempeño según la métrica elegida es el modelo que incluye
todos los regresores de dicho conjunto.
