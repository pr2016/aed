% Tercer Set
% Carlos Pita
% 20 de Abril de 2016

# Ejercicio 2.1

## Ajustes sobre el total de datos

La figura \ref{fig:all} muestra el diagrama de dispersión con las rectas de
regresión para cada uno de los modelos pedidos. El primer modelo corresponde a
una función constante (solo presenta un término fijo o *intercept*) y, por
consiguiente, aparece como una horizontal en la figura. Las rectas para los
otros dos modelos son muy similares entre sí; sin embargo, aunque no se aprecie
en la figura, la recta correspondiente al segundo modelo pasa exactamente por el
origen, debido a que carece de término fijo. La siguiente tabla resume los
resultados pedidos para cada modelo:

  Modelo     Término fijo     Pendiente    Predicción
----------  -------------    ----------   -----------
   1               8.2473        ---           8.2473
   2                 ---      1.3216e-4        6.6079
   3               2.0861     1.0210e-4        7.1912

## Ajustes sobre el conjunto de entrenamiento

Podemos comparar los tres modelos, al menos desde el punto de vista de sus
errores de predicción, tomando un conjunto de entrenamiento al azar y estimando
el error cuadrático medio de predicción sobre el resto de las observaciones (es
decir, sobre el conjunto de test). Las rectas de regresión resultantes al
estimar sobre el conjunto de entrenamiento se presentan en la figura
\ref{fig:train}. La siguiente tabla resume los resultados pedidos para cada
modelo:\newpage

  Modelo      Término fijo     Pendiente    Predicción       Error
----------   -------------    ----------   -----------   ---------
    1               8.1731       ---            8.1731     10.8594
    2                ---       1.3320e-4        6.6600      4.6267
    3               2.3853     0.9835e-4        7.3027      4.6215

## Selección de modelo

Si bien la diferencia entre los errores de los modelos 2 y 3 no es muy
importante (como cabía esperar, dada la similitud entre sus rectas de
regresión), el modelo 3 presenta un error menor. Por otra parte, el error de
predicción estimado para el modelo 1 es claramente superior al de los otros dos
modelos, hecho que también resulta obvio a partir del examen de las figuras
anteriores. En consecuencia, selecciono el modelo número 3. Cabe aclarar que,
aunque este modelo presente más grados de libertad que los otros dos, no siempre
esto garantiza un error de predicción menor (el error de predicción depende de
un trade off entre sesgo y varianza que no siempre mejora al agregar grados de
libertad al modelo). Finalmente, quiero señalar que, para la corrida presentada
en este informe, fijé la semilla del generador de números aleatorios del
sistema; no obstante, diferentes corridas con distintas semillas presentan
resultados ambiguos: frecuentemente el modelo 2 "vence" al modelo 3.

![Ajustes sobre el total de datos.\label{fig:all}](all.png)

![Ajustes sobre el conjunto de entrenamiento.\label{fig:train}](train.png)
