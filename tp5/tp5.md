% Quinto Set
% Carlos Pita
% 11 de Mayo de 2016

# Ejercicio 1

Para el esquema de validación cruzada se emplearon 5 folds. Antes de particionar
los datos en folds, se practicó un *shuffle* sobre los mismos, a efectos de
eliminar cualquier componente sistemático que pudiera existir en el ordenamiento
original. Al realizar este reordenamiento se configuró, para cada modelo
evaluado, la misma semilla al generador de numeros aleatorios, a fin de
preservar los folds entre los distintos modelos. Los errores cuadráticos medios
de predicción estimados por validación cruzada se listan en la siguiente tabla:

Modelo                                             ECM
---------------------------------------     ----------
`calidad ~  1`                                 9.92105
`calidad ~ -1 + precio`                        4.44328
`calidad ~  1 + precio`                        4.06759
`calidad ~  1 + precio + I(precio^2)`          4.04815

Por una ligera diferencia, el cuarto modelo resulta ganador. El modelo se
reestimó sobre el total de los datos. La figura \ref{fig:win} presenta los datos
originales junto a la curva de ajuste del modelo ganador.

![Ajuste del modelo ganador.\label{fig:win}](autos.png)

\newpage

# Ejercicio 5

Para cada medida de posición se generaron 1000 muestras con reposición, de 100
elementos cada una. Como antes, se reinició la semilla del generador de números
aleatorios para cada medida, a fin de preservar las muestras entre medidas. La
media resultó ser la medida de posición con menor desvío estándar, como se
presenta en la siguiente tabla:

Medida                    Desvío estándar
-------------------    ------------------
Media                            0.818345
Mediana                          1.412679
Media 0.1-podada                 0.873219
