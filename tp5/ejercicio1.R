k.fold <- function(form, data, y, k, seed=0) {
    set.seed(seed)
    n <- nrow(data)
    folds <- split(sample(1:n), cut(1:n, k))
    mse <- 0
    for (i in 1:k) {
        test <- folds[[i]]
        train <- do.call(c, folds[-i])
        model <- lm(form, data[train,])
        mse <- mse + sum((y[test] - predict(model, data[test,]))^2)
    }
    mse / n
}

plot.model <- function(name, form, data, x, y) {
    jpeg(paste(name, '.png', sep=''), width=1024, height=480)
    plot(x, y)
    model <- lm(form, data)
    lines(sort(x), predict(model, data[order(x),]))
    dev.off()
}

k <- 5
autos <- read.table('autos.txt', header=T)
autos.k.fold <- function(item, form) {
    cat(item, ') ', k.fold(form, autos, autos$calidad, k), '\n', sep='')
}

autos.k.fold('a', calidad ~  1)
autos.k.fold('b', calidad ~ -1 + precio)
autos.k.fold('c', calidad ~  1 + precio)
autos.k.fold('d', calidad ~  1 + precio + I(precio^2))

plot.model('autos', calidad ~ 1 + precio + I(precio^2),
           autos, autos$precio, autos$calidad)
