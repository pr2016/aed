fe2 <- c(4.04, 4.16, 4.42, 4.93, 5.49, 5.77, 5.86, 6.28, 6.97, 7.06, 7.78, 9.23,
         9.34, 9.91, 13.46, 18.40, 23.89, 26.39)
lfe2 <- log(fe2)

fe3 <- c(2.20, 2.93, 3.08, 3.49, 4.11, 4.95, 5.16, 5.54, 5.68, 6.25, 7.25, 7.90,
         8.85, 11.96, 15.54, 15.89, 18.30, 18.59)
lfe3 <- log(fe3)

plot.fe <- function(x, file='') {
    if (file != '') jpeg(file, width=1024, height=480)
    par(mfrow=c(1, 3))
    hist(x, freq=F, main='Histograma')
    lines(density(x))
    qqnorm(x, main='QQ vs. normal' )
    qqline(x)
    boxplot(x, main='Boxplot')
    if (file != '') dev.off()
}

plot.fe(fe2, 'fe2.jpg')
plot.fe(lfe2, 'lfe2.jpg')
plot.fe(fe3, 'fe3.jpg')
plot.fe(lfe3, 'lfe3.jpg')
