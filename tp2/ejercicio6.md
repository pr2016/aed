% Segundo Set
% Carlos Pita
% 13 de Abril de 2016

# Ejercicio 2.6

## Sin transformación

En la figura \ref{fig:fe2} se presentan los gráficos pedidos para la muestra
correspondiente a Fe2+. Vemos en el histograma una cola demasiado larga a
derecha. Esta asimetría positiva no corresponde a una distribución normal, que
es simétrica. La misma característica se revela en el gráfico cuantil-cuantil
contra la distribución normal; en el mismo se puede deducir la existencia de la
cola larga a partir del subconjunto de puntos que aparecen muy por encima del
segmento rectilíneo, sobre el cual coincidirían ambas distribuciones (la
observada y la normal). Asimismo, el boxplot muestra "bigotes" asimétricos y la
presencia de outliers a derecha (es decir, extremadamente grandes).

![Plots para Fe2+.\label{fig:fe2}](fe2.jpg)

La figura \ref{fig:fe3} permite sacar conclusiones similares para la muestra
correspondiente a Fe3+. Aquí tampoco es verosímil que la distribución empírica
corresponda a una normal, debido a la existencia de una asimetría del mismo
signo, si bien más moderada que la observada para Fe2+. Adicionalmente, la
distribución empírica presenta una ligera bimodalidad, también incompatible con
la hipótesis de normalidad.

![Plots para Fe3+.\label{fig:fe3}](fe3.jpg)

## Con transformación logarítmica

El tipo de cola larga a derecha sugiere la aplicación de una transformación
logarítmica para volver más simétrica la distribución. En las figuras
\ref{fig:lfe2} y \ref{fig:lfe3}, correspondientes a las muestras transformadas,
podemos observar que, si bien la asimetría no fue completamente eliminada, al
menos sus rasgos más extremos sí lo fueron. La aproximación normal resulta un
poco más verosímil después de la transformación y una comparación entre las
muestras empleando los procedimientos clásicos de inferencia estadística sería
un poco más legítima que antes, sobre todo en el caso de Fe3+.

![Plots para $log$(Fe2+).\label{fig:lfe2}](lfe2.jpg)

![Plots para $log$(Fe3+).\label{fig:lfe3}](lfe3.jpg)
