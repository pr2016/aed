% Sexto Set
% Carlos Pita
% 24 de Mayo de 2016

# Ejercicio 6

## Punto b

En la figura \ref{fig:f} se muestra un diagrama de dispersión para los puntos
generados, junto con la curva que representa la parte sistemática de la función
simulada (*i.e.* la media, sin considerar el error normal).

![Simulación.\label{fig:f}](f.png)

## Punto c

Tomo 5 folds ($k = 5$) y considero para $h$ una grilla de 100 puntos
equidistantes entre sí, desde 0.1 hasta 2, empleando en todos los casos el
núcleo $K(z) = I\{z \le 1\}$. En este rango, el valor de $h$ que minimiza el
error cuadrático medio de predicción estimado por CV es $h = 0.426$. La figura
\ref{fig:errs} muestra los errores estimados para todos los valores de la
grilla^[En muy raras ocasiones, para valores pequeños de $h$, el número de
vecinos seleccionados por el núcleo resultó ser 0, provocando que la estimación
del error quedara completamente invalidada por una o dos observaciones. Dado que
estos casos fueron contados, preferí excluirlos del cálculo del error promedio a
invalidar el mismo.]

![Errores estimados por CV.\label{fig:errs}](errs.png)

## Punto d

La figura \ref{fig:np} muestra el resultado de la regresión no paramétrica para
el $h$ óptimo superpuesto a la salida de la figura \ref{fig:f}. Se observa que
la curva estimada no es suave pero aproxima muy bien la función original.

![Regresión no paramétrica.\label{fig:np}](np.png)

## Punto e

Las figuras \ref{fig:lm1}, \ref{fig:lm2} y \ref{fig:lm3} superponen (en rojo)
los resultados de las regresiones lineales para los modelos lineales pedidos a
la salida de la figura \ref{fig:np}. Las curvas estimadas son suaves; la
aproximación de grado 1 es muy burda pero la aproximación de grado 3 es casi
perfecta.


![Modelo lineal de grado 1.\label{fig:lm1}](lm1.png)

![Modelo lineal de grado 2.\label{fig:lm2}](lm2.png)

![Modelo lineal de grado 3.\label{fig:lm3}](lm3.png)

## Punto f

En general, me quedaría con un modelo paramétrico con una cantidad "razonable"
de grados de libertad^[Qué es "razonable" podría justificarse por
consideraciones teóricas y/o mediante métodos de remuestreo.], porque ofrece
ventajas analíticas obvias (el modelo es mucho más tratable) y un mayor
resguardo contra el sobre-ajuste. Es cierto que un modelo paramétrico puede
imponer restricciones arbitrarias a la forma funcional, que no son parte del
dominio del problema; pero si estas restricciones pueden justificarse a partir
de la estructura del problema, un modelo paramétrico apropiado incluirá
información *a priori* sobre el problema que un modelo no paramétrico solo puede
aspirar a capturar de los datos.

En el caso presente, conociendo la forma del proceso generador, resulta más o
menos evidente que un polinomio de grado 3 tiene la flexibilidad suficiente para
ajustarse a los datos dentro del rango pedido. Sin embargo, para justificar la
elección de uno de los cuatro modelos en base a los datos, habría que estimar
sus correspondientes errores de predicción por algún método de remuestreo y
elegir el modelo que minimice el error. En este ejercicio lo hicimos solamente
para una familia de modelos no paramétricos parametrizados en $h$, pero no para
los modelos lineales.
